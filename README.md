# Angular5 demo

## 文件结构

- [app](src/app) 源码目录
  - [app.module.ts](src/app/app.module.ts) 主模块
  - [app-routing.module.ts](src/app/app-routing.module.ts) 主路由
  - [shared](src/app/shared) 通用模块
    - [layout](src/app/shared/layout) 布局组件
    - [header](src/app/shared/header) 头部组件
    - [content](src/app/shared/content) 内容组件
    - [footer](src/app/shared/footer) 底部组件
    - [exception](src/app/shared/exception) 异常处理
      - [page-not-fount](src/app/shared/exception/page-not-fount) 404页面
    - [pipe](src/app/shared/pipe) 通用过滤器
      - [shared-pipe.module.ts](src/app/shared/pipe/shared-pipe.module.ts) 通用过滤器模块引入文件
      - [sex.pipe.ts](src/app/shared/pipe/sex.pipe.ts) 性别过滤器
    - [class](src/app/shared/class) 通用类
      - [param.ts](src/app/shared/class/param.ts) http请求参数
      - [result.ts](src/app/shared/class/result.ts) http请求结果
      - [page-result.ts](src/app/shared/class/page-result.ts) 分页参数
    - [index](src/app/shared/index) 主页组件
  - [modules](src/app/modules) 业务模块
    - [user](src/app/modules/user) 用户模块
    - [role](src/app/modules/role) 角色模块
    - [order](src/app/modules/order) 订单模块
- [assets](src/assets) 静态文件目录
- [environments](src/environments) 环境配置文件目录
  - [environment.ts](src/environments/environment.ts) 本地开发环境配置
  - [environment.prod.ts](src/environments/environment.prod.ts) 生产环境配置
- [styles](src/styles) 样式目录
- [styles.scss](src/styles.scss) 样式主文件

## 使用技术

- [Angular 5.2.0](https://angular.cn/) Angular 5.x
- [ng-zorro 0.7.1](https://ng.ant.design/docs/introduce/zh) 基于Angular的UI库
- [underscore 1.9.0](http://underscorejs.org) 非常好用的js工具库
- [ngx-cookie-service 1.0.10](https://github.com/7leads/ngx-cookie-service) 操作cookie工具类

## 单点登陆服务器

[cloud-demo](https://github.com/qq253498229/cloud-demo)
