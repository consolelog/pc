import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {IndexComponent} from './shared/index/index.component';
import {PageNotFountComponent} from './shared/exception/page-not-fount/page-not-fount.component';
import {LayoutComponent} from './shared/layout/layout.component';
import {AuthInterceptor} from './shared/auth/auth.interceptor';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthGuard} from './shared/auth/auth.guard';
import {AuthService} from './shared/auth/auth.service';

const appRoutes: Routes = [
  {
    path: '', component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {path: '', redirectTo: '/index', pathMatch: 'full'},
      {path: 'index', component: IndexComponent},
      {path: 'user', loadChildren: 'app/modules/user/user.module#UserModule'},
      {path: 'role', loadChildren: 'app/modules/role/role.module#RoleModule'},
      {path: 'menu', loadChildren: 'app/modules/menu/menu.module#MenuModule'},
      {path: 'order', loadChildren: 'app/modules/order/order.module#OrderModule'},
    ]
  },
  {path: '**', component: PageNotFountComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      // {enableTracing: true}
    )
  ],
  exports: [
    RouterModule
  ],
  providers: [
    AuthGuard,
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    }
  ]
})
export class AppRoutingModule {
}
