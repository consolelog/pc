import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';

import {registerLocaleData} from '@angular/common';
import zh from '@angular/common/locales/zh';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {IndexComponent} from './shared/index/index.component';
import {HeaderComponent} from './shared/header/header.component';
import {ContentComponent} from './shared/content/content.component';
import {LayoutComponent} from './shared/layout/layout.component';
import {FooterComponent} from './shared/footer/footer.component';
import {PageNotFountComponent} from './shared/exception/page-not-fount/page-not-fount.component';
import {SharedPipeModule} from './shared/pipe/shared-pipe.module';
import {CookieService} from 'ngx-cookie-service';

registerLocaleData(zh);

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    PageNotFountComponent,
    HeaderComponent,
    FooterComponent,
    ContentComponent,
    LayoutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SharedPipeModule,
    NgZorroAntdModule.forRoot(),
    AppRoutingModule,
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
