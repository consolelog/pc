import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DetailComponent} from './detail/detail.component';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: '', component: DetailComponent},
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetailComponent],
  exports: [RouterModule]
})
export class MenuModule {
}
