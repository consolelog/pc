export class User {
  user_name: string;
  exp: string;
  jti: string;
  client_id: string;
  authorities: string[];
  scope: string[];


  constructor(
    options: {
      user_name?: string;
      exp?: string;
      jti?: string;
      client_id?: string;
      authorities?: string[];
      scope?: string[];
    } = {}
  ) {
    this.user_name = options.user_name;
    this.exp = options.exp;
    this.jti = options.jti;
    this.client_id = options.client_id;
    this.authorities = options.authorities || [];
    this.scope = options.scope || [];
  }
}
