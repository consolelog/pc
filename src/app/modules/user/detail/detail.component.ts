import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../user.service';
import {User} from '../class/user';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  id: any;

  model: User = new User();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: UserService,
    private message: NzMessageService
  ) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id && this.id !== 'new') {
      this.service.getUser(this.id).subscribe(res => {
        this.model = res;
      });
    }
  }

  submitForm() {
    this.message.success('保存成功');
    this.router.navigate(['/user']);
  }

  back() {
    this.router.navigate(['/user']);
  }
}
