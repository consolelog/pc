import {Component, OnInit} from '@angular/core';
import {NzMessageService} from 'ng-zorro-antd';
import * as _ from 'underscore';
import {UserService} from '../user.service';
import {Param} from '../../../shared/class/param';
import {Result} from '../../../shared/class/result';
import {PageResult} from '../../../shared/class/page-result';
import {Router} from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  param: Param = new Param({
    pageNo: 1,
    pageSize: 10,
    map: {
      username: ''
    }
  });

  result: Result = new Result({data: new PageResult()});

  constructor(
    private message: NzMessageService,
    private service: UserService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.service.getUserList(this.param).subscribe(res => {
      this.result = new Result(res);
    });
  }

  delete(id) {
    this.result.data.list = _.filter(this.result.data.list, res => res['id'] !== id);
    this.message.success('删除成功', {nzDuration: 2000});
  }

  pageChange(pageNo: number) {
    this.param.pageNo = pageNo;
    this.ngOnInit();
  }

  edit(id: any) {
    this.router.navigate(['/user', id]);
  }

  new() {
    this.router.navigate(['/user', 'new']);
  }
}
