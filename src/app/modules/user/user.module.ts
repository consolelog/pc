import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListComponent} from './list/list.component';
import {DetailComponent} from './detail/detail.component';
import {RouterModule, Routes} from '@angular/router';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {FormsModule} from '@angular/forms';
import {SharedPipeModule} from '../../shared/pipe/shared-pipe.module';
import {UserService} from './user.service';

const routes: Routes = [
  {path: '', component: ListComponent},
  {path: ':id', component: DetailComponent},
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgZorroAntdModule,
    RouterModule.forChild(routes),
    SharedPipeModule
  ],
  declarations: [ListComponent, DetailComponent],
  exports: [RouterModule],
  providers: [UserService]
})
export class UserModule {
}
