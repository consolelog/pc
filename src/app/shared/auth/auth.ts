export class Auth {
  access_token: string;
  expires_to: number;
  jti: string;
  refresh_token: string;
  scope: string;
  token_type: string;


  constructor(
    options: {
      access_token?: string;
      expires_to?: number;
      jti?: string;
      refresh_token?: string;
      scope?: string;
      token_type?: string;
    } = {}
  ) {
    this.access_token = options.access_token;
    this.expires_to = options.expires_to;
    this.jti = options.jti;
    this.refresh_token = options.refresh_token;
    this.scope = options.scope;
    this.token_type = options.token_type;
  }
}
