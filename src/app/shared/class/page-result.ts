export class PageResult {
  list: any[];
  count: number;


  constructor(options: {
    list: any[];
    count: number;
  } = {} as  {
    list: any[];
    count: number;
  }) {
    this.list = options.list || [];
    this.count = options.count || 0;
  }
}
