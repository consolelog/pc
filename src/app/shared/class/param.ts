export class Param {
  /**
   * 页码
   */
  pageNo: number;
  /**
   * 每页条数
   */
  pageSize: number;
  /**
   * 参数
   */
  map: any;


  constructor(options: {
    pageNo?: number,
    pageSize?: number,
    map?: any
  } = {}) {
    this.pageNo = options.pageNo || 1;
    this.pageSize = options.pageSize || 10;
    this.map = options.map || {};
  }
}
