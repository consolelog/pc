export class Result {
  code: number;
  msg: string;
  data: any;


  constructor(options: {
    code?: number;
    msg?: string;
    data?: any;
  } = {}) {
    this.code = options.code;
    this.msg = options.msg;
    this.data = options.data || {};
  }
}
