import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'sex'
})
export class SexPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value != null && value !== undefined && value === 1) {
      return '男';
    }
    if (value != null && value !== undefined && value === 0) {
      return '女';
    }
    return '其它';
  }

}
