import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SexPipe} from './sex.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    SexPipe
  ],
  exports: [
    SexPipe
  ]
})
export class SharedPipeModule {
}
